import './App.css';
import NavContainer from './containers/NavContainer';
import LoginContainer from './containers/LoginContainer';
import Register from './components/Register';
import Contact from './components/Contact';
import Addcontact from './components/Addcontact';
import Editcontact from './components/Editcontact'
import Dashboard from './components/Dashboard'
import Protected from './components/Protected'
import LogoutContainer from './containers/LogoutContainer'
import { BrowserRouter as Router, Route, Switch, useHistory } from 'react-router-dom';
import React, { useEffect, useState } from 'react';




function App() {
       

  return (
    <div className="App">
      <Router>
      <NavContainer />
        <Switch>
          <Route exact path="/login" component={LoginContainer}></Route> 
          <Route exact path="/register" component={Register}></Route>
          <Route exact path="/contact"><Protected component={Contact} /></Route> 
          <Route exact path="/addcontact" ><Protected component={Addcontact} /></Route> 
          <Route exact path="/editcontact/:contactId"><Protected component={Editcontact} /></Route> 
          <Route exact path="/dashboard"><Protected component={Dashboard} /></Route>
          <Route exact path="/logout" component={LogoutContainer}></Route> 
          {/* <Route exact path="/*"></Route> */}
        </Switch>
      </Router>

    </div>
  );
}

export default App;
