import axios from 'axios'
import {useParams} from "react-router-dom";
import {useEffect, useState} from "react";
import {useHistory} from 'react-router-dom'
import {toast} from 'react-toastify';
import Spinner from './UI/Spinner'
import 'react-toastify/dist/ReactToastify.css';
toast.configure()
function Editcontact(props){
   const [fist,setFirst]=useState()
   const [lastName,setLast]=useState()
   const [contactEmail,setEmail]=useState()
   const [contactPhone,setPhone]=useState()
   const [contactNick,setNick]=useState()
   const [contactCompany,setCompany]=useState()
   const [oldPhone,setOldPhone]=useState()
   const [id,setId]=useState()
   const query=useParams();
   const history=useHistory();
   const [error,setError]=useState({})
   const [loading,setLoading]=useState(true)
  
   useEffect(()=>{
       axios({
        url: process.env.REACT_APP_BASE_URL+'/contact/'+query.contactId+'/edit',
        method: 'get',
        headers: {
            Authorization: 'Bearer ' + localStorage.token,
        }
       }).then((response)=>{
           if(response.data.error_type=='404'){
            toast.warning("Sorry! No record found") 
            props.history.push('/contact')
           }
           else{
               let data=response.data.data
               setFirst(data.first_name)
               setLast(data.last_name)
               setEmail(data.contact_email)
               setPhone(data.contact_phone_number)
               setNick(data.contact_nickname)
               setCompany(data.contact_company)
               setOldPhone(data.contact_phone_number)
               setId(data.contactId)
               
           }
           setLoading(false)

       },(error)=>{
           console.log(error)
       })
   },[query.contactId])

   const contactHandler=async (e)=>{
    e.preventDefault();
    var errors={}
    if(fist===''){
      errors.first="First name is required"
    }
    if(lastName===''){
        errors.last="Last name is required"
    }
    if(contactNick===''){
        errors.nick="Nick name is required"
    }
    if(contactCompany===''){
        errors.company="Company name is required"
    }
    if(contactEmail===''){
        errors.email='Email is required'
    }
    else if(contactEmail){
        const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
            var isValid = pattern.test(contactEmail);
            if (!isValid) {
               errors.email='Please enter a valid email address'
            }
    }
    if(contactPhone===''){
        errors.phone='Phone is required'
    }
    else if(contactPhone){
        if(isNaN(contactPhone)){
            errors.phone='Please enter phone number in digits'
        }
    }
    var errorKeys = Object.keys(errors);
        if (errorKeys.length > 0) {
         setError(errors)
        } else {
            setLoading(true)
         const response=await  axios({
            url:process.env.REACT_APP_BASE_URL+'/contact/'+id,
            method:"PUT",
            headers: {
                Authorization: 'Bearer ' + localStorage.token,
            },
            data:{
                'userid':localStorage.userid,
                'first_name': fist,
                'last_name': lastName,
                'email': contactEmail,
                'phone': contactPhone,
                'nickname':contactNick,
                'company':contactCompany,
                'address':'Test address',
                'oldPhone':oldPhone,
                'status':1
            }
           })
            if(response.data.error_type==='validation'){
                toast.warning("Contact already exist with this number") 
                setLoading(false)
            }
            else if(response.data.error_type==='none'){
                toast.success("Contact has been updated successfully") 
                setLoading(false)
                history.push('/contact')
            }
           
           
        }
    setError(errors)

}
 




   return (
<div>
    {loading?(<Spinner/>):(
    <main className="py-4">
    <div className="container">
        <div className="row justify-content-center">
            <div className="col-md-8">
                <div className="card">
                    <div className="card-header">Edit Contact</div>

                    <div className="card-body">
                        <form onSubmit={contactHandler}>

                            <div className="form-group row">
                                <label for="first" className="col-md-4 col-form-label text-md-right">First Name</label>
                                <div className="col-md-6">
                                    <input type="text" className="form-control" autoFocus onChange={(e)=>setFirst(e.target.value)} value={fist}  />
                                    <span style={{ color: "red" }}>{error?.first}</span>
                                </div>
                            </div>

                            <div className="form-group row">
                                <label for="last" className="col-md-4 col-form-label text-md-right">Last Name</label>
                                <div className="col-md-6">
                                    <input type="text" className="form-control " onChange={(e)=>setLast(e.target.value)} value={lastName} />
                                    <span style={{ color: "red" }}>{error?.last}</span>
                                </div>
                            </div>

                            <div className="form-group row">
                                <label for="email" className="col-md-4 col-form-label text-md-right">E-Mail Address</label>
                                <div className="col-md-6">
                                    <input type="email" className="form-control " onChange={(e)=>setEmail(e.target.value)} value={contactEmail} />
                                    <span style={{ color: "red" }}>{error?.email}</span>
                                </div>
                            </div>

                            <div className="form-group row">
                                <label for="phone" className="col-md-4 col-form-label text-md-right">Phone no.</label>
                                <div className="col-md-6">
                                    <input type="text" className="form-control " onChange={(e)=>setPhone(e.target.value)} value={contactPhone}/>
                                    <span style={{ color: "red" }}>{error?.phone}</span>
                                </div>
                            </div>

                            <div className="form-group row">
                                <label for="password" className="col-md-4 col-form-label text-md-right">Nick Name</label>
                                <div className="col-md-6">
                                    <input type="text" className="form-control " onChange={(e)=>setNick(e.target.value)}  value={contactNick} />
                                    <span style={{ color: "red" }}>{error?.nick}</span>
                                </div>
                            </div>

                            <div className="form-group row">
                                <label for="password" className="col-md-4 col-form-label text-md-right">Company</label>
                                <div className="col-md-6">
                                    <input type="text" className="form-control " onChange={(e)=>setCompany(e.target.value)} value={contactCompany} />
                                    <span style={{ color: "red" }}>{error?.company}</span>
                                </div>
                            </div>


                            <div className="form-group row mb-0">
                                <div className="col-md-8 offset-md-4">
                                    <button type="submit" className="btn btn-primary">
                                        Update
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
)}
</div>
   )
}

export default Editcontact