import { useState, useEffect } from 'react';
import axios from 'axios'
import { toast } from 'react-toastify';
import Spinner from "./UI/Spinner";
import 'react-toastify/dist/ReactToastify.css';
toast.configure()

function Login(props) {

    // if(localStorage.token && localStorage.userid){
    //     props.history.push('/dashboard');
    // }
    useEffect(() => {
        if (localStorage.token && localStorage.userid) {
            props.history.push('/dashboard');
        }
    }, [])

    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const [error, setError] = useState({})
    const [loading,setLoading]=useState(false)

    const usernameHandler = (e) => {
        let username = e.target.value
        if (username === '') {
            setError({
                ...error,
                usernameErr: 'Please enter a username'
            })
        }
        else {
            setError({
                ...error, usernameErr: ''
            })
        }
        setUsername(username)
    }

    const passwordHanlder = (e) => {
        let password = e.target.value
        if (password === '') {
            setError({
                ...error,
                passwordErr: 'Please enter password'
            })
        }
        else {
            setError({
                ...error,
                passwordErr: ''
            })
        }
        setPassword(password)
    }

    const validate = (elements) => {
        var errors = {};

        if (!elements.email.value) {
            errors.email = "Plaese enter email";
        } else if (elements.email.value) {
            const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
            var isValid = pattern.test(elements.email.value);
            if (!isValid) {
                errors.email = "Plaese enter valid email";
            }
        }
        if (!elements.password.value) {
            errors.password = "Plaese enter password";
        }
        var errorKeys = Object.keys(errors);
        if (errorKeys.length > 0) {
            return errors;
        } else {
            return false;
        }
    };
     const loginHandler = async (e) => {
        e.preventDefault();
        var form = document.getElementById("loginform");
        var errors = {};
        if (!username) {
            errors.email = "Plaese enter email";
        }
        if (!password) {
            errors.password = "Plaese enter password";
        }

        var errorKeys = Object.keys(errors);
        if (errorKeys.length > 0) {
            setError(errors)
        }
        else {
            //api call goes here
            setLoading(true)
          const response= await axios({
                url: process.env.REACT_APP_BASE_URL + '/signin',
                method: "post",
                data: { 'username': username, 'password': password }
            })
                if (response.data.message) {
                    toast.error(response.data.message)
                    setLoading(false)
                }
                else {
                    console.log(response)
                    localStorage.token = response.data.token
                    localStorage.userid = response.data.user.id
                    toast.success('Login Successfully')
                    // setLogin(true)
                    props.loginData({ token: localStorage.token, userid: localStorage.userid })
                    setLoading(false)
                   // props.history.push('/dashboard')
                    props.history.replace('/dashboard')

                }
               // const ayush =await response.data;
             
        }
    }

    return (
<div>
{loading ? (
        <Spinner />
      ) : (
        <main className="py-4">
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-8">
                        <div className="card">
                            <div className="card-header">Login</div>

                            <div className="card-body">
                                <form id="loginform" onSubmit={loginHandler}>

                                    <div className="form-group row">
                                        <label for="username" className="col-md-4 col-form-label text-md-right">E-Mail Address/Phone no.</label>

                                        <div className="col-md-6">
                                            <input type="text" className="form-control" name="email" onChange={usernameHandler} />
                                            <span style={{ color: "red" }}>{error?.email}</span>
                                        </div>

                                    </div>

                                    <div className="form-group row">
                                        <label for="password" className="col-md-4 col-form-label text-md-right">Password</label>

                                        <div className="col-md-6">
                                            <input type="password" className="form-control " name="password" onChange={passwordHanlder} />
                                            <span style={{ color: "red" }}>{error?.password}</span>
                                        </div>

                                    </div>


                                    <div className="form-group row mb-0">
                                        <div className="col-md-8 offset-md-4">
                                            <button type="submit" className="btn btn-primary" >
                                                Login
                                            </button>


                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>)}
        </div>
    )
}

export default Login;