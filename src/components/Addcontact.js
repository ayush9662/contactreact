import axios from "axios"
import {useState} from 'react'
import {useHistory} from 'react-router-dom'
import {toast} from 'react-toastify';
import Spinner from './UI/Spinner'
import 'react-toastify/dist/ReactToastify.css';
toast.configure()

function Addcontact(props){
    const history=useHistory();
const [contact,addContact]=useState({first:"",last:"",email:"",phone:"",nick:"",company:""})
const [error,setError]=useState({})
const [loading, setLoading] = useState(false)

const first=(e)=>{
    if(e.target.value===''){
        setError({...error,first:'First name is required'})
    }
    else{
        setError({...error,first:''})
    }
    addContact({...contact,first:e.target.value})
}

const last=(e)=>{
    if(e.target.value===''){
        setError({...error,last:'Last name is required'})
    }
    else{
        setError({...error,last:''})
    }
    addContact({...contact,last:e.target.value})
}

const email=(e)=>{
    if(e.target.value===''){
        setError({...error,email:'Email is required'})
    }
    else if(e.target.value){
        const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
            var isValid = pattern.test(e.target.value);
            if (!isValid) {
                setError({...error,email:'Please enter a valid email address'})
            }
    }
    else{
        setError({...error,email:''})
    }
    addContact({...contact,email:e.target.value})
}

const phone=(e)=>{
    if(e.target.value===''){
        setError({...error,phone:'Phone is required'})
    }
    else if(e.target.value){
        if(isNaN(e.target.value)){
            setError({...error,phone:'Please enter phone number in digits'})  
        }
    }
    else{
        setError({...error,phone:''})
    }
    addContact({...contact,phone:e.target.value})
}

const company=(e)=>{
    if(e.target.value===''){
        setError({...error,company:'Company field is required'})
    }
    else{
        setError({...error,company:''})
    }
    addContact({...contact,company:e.target.value})
}

const nick=(e)=>{
    if(e.target.value===''){
        setError({...error,nick:'Nickname field is required'})
    }
    else{
        setError({...error,nick:''})
    }
    addContact({...contact,nick:e.target.value})
}

const contactHandler=async (e)=>{
    e.preventDefault();
    var errors={}
    if(contact.first===''){
      errors.first="First name is required"
    }
    if(contact.last===''){
        errors.last="Last name is required"
    }
    if(contact.nick===''){
        errors.nick="Nick name is required"
    }
    if(contact.company===''){
        errors.company="Company name is required"
    }
    if(contact.email===''){
        errors.email='Email is required'
    }
    else if(contact.email){
        const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
            var isValid = pattern.test(contact.email);
            if (!isValid) {
               errors.email='Please enter a valid email address'
            }
    }
    if(contact.phone===''){
        errors.phone='Phone is required'
    }
    else if(contact.phone){
        if(isNaN(contact.phone)){
            errors.phone='Please enter phone number in digits'
        }
    }
    var errorKeys = Object.keys(errors);
        if (errorKeys.length > 0) {
         setError(errors)
        } else {
            setLoading(true)
           const response=await axios({
                url:process.env.REACT_APP_BASE_URL+'/contact',
                method:"POST",
                headers: {
                    Authorization: 'Bearer ' + localStorage.token,
                },
                data:{
                    'userid':localStorage.userid,
                    'first_name': contact.first,
                    'last_name': contact.last,
                    'email': contact.email,
                    'phone': contact.phone,
                    'nickname':contact.nick,
                    'company':contact.company,
                    'address':'Test address',
                    'status':1
                }
            })
              
                if(response.data.error_type==='validation'){
                    toast.warning("Contact already exist with this number") 
                    setLoading(false)
                }
                else if(response.data.error_type==='none'){
                    toast.success("Contact has been added successfully") 
                    setLoading(false)
                    history.push('/contact')
                }
            
        }
    setError(errors)

}

    return(
    <div>
        {loading?(<Spinner/>):(

        <main className="py-4">
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-8">
                        <div className="card">
                            <div className="card-header">Add Contact</div>

                            <div className="card-body">
                                <form onSubmit={contactHandler}>

                                    <div className="form-group row">
                                        <label for="first" className="col-md-4 col-form-label text-md-right">First Name</label>
                                        <div className="col-md-6">
                                            <input type="text" className="form-control" autofocus onChange={first} />
                                            <span style={{ color: "red" }}>{error?.first}</span>
                                        </div>
                                    </div>

                                    <div className="form-group row">
                                        <label for="last" className="col-md-4 col-form-label text-md-right">Last Name</label>
                                        <div className="col-md-6">
                                            <input type="text" className="form-control " onChange={last}  />
                                            <span style={{ color: "red" }}>{error?.last}</span>
                                        </div>
                                    </div>

                                    <div className="form-group row">
                                        <label for="email" className="col-md-4 col-form-label text-md-right">E-Mail Address</label>
                                        <div className="col-md-6">
                                            <input type="email" className="form-control " onChange={email} />
                                            <span style={{ color: "red" }}>{error?.email}</span>
                                        </div>
                                    </div>

                                    <div className="form-group row">
                                        <label for="phone" className="col-md-4 col-form-label text-md-right">Phone no.</label>
                                        <div className="col-md-6">
                                            <input type="text" className="form-control " onChange={phone} />
                                            <span style={{ color: "red" }}>{error?.phone}</span>
                                        </div>
                                    </div>

                                    <div className="form-group row">
                                        <label for="password" className="col-md-4 col-form-label text-md-right">Nick Name</label>
                                        <div className="col-md-6">
                                            <input type="text" className="form-control " onChange={nick} />
                                            <span style={{ color: "red" }}>{error?.nick}</span>
                                        </div>
                                    </div>

                                    <div className="form-group row">
                                        <label for="password" className="col-md-4 col-form-label text-md-right">Company</label>
                                        <div className="col-md-6">
                                            <input type="text" className="form-control " onChange={company} />
                                            <span style={{ color: "red" }}>{error?.company}</span>
                                        </div>
                                    </div>


                                    <div className="form-group row mb-0">
                                        <div className="col-md-8 offset-md-4">
                                            <button type="submit" className="btn btn-primary">
                                                Add
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        )}
        </div>
    )
}

export default Addcontact;