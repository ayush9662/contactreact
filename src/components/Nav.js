import { useEffect } from 'react';
import { Link,useHistory } from 'react-router-dom';
import { login } from '../Service/actions/action';
function Nav(props) {
  
   const loginCheck=props.data.authReducer
   const redirect=useHistory()
   let currentTime=new Date()
   useEffect(()=>{
       if(loginCheck.expireTime<currentTime.getTime()){
           redirect.replace('/logout')
       }
       

   },[localStorage.token])

    return (
        

        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <a className="navbar-brand" href="#">Cloud Contact</a>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav mr-auto">
                {loginCheck.isLoggedIn && <Link to="/contact">
                  Contact 
                </Link>}
                {loginCheck.isLoggedIn &&  <Link to="/logout">
                 | Logout
                </Link>}
                {!loginCheck.isLoggedIn &&  <Link to="/login">Login</Link>}
                
                {!loginCheck.isLoggedIn &&  <Link to="/register"> | Register</Link>}
                </ul>

            </div>
        </nav>
    )
}

export default Nav;