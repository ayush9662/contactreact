import { Link } from "react-router-dom"
import axios from 'axios'
import { useEffect, useState } from 'react'
import { MDBDataTable } from 'mdbreact';
import Spinner from './UI/Spinner'

function Contact() {
    const [contact, setContact] = useState([])
    const [loading, setLoading] = useState(true)
    const [data, setData] = useState({})
    useEffect(() => {
        axios({
            url: process.env.REACT_APP_BASE_URL + '/contact?userid=' + localStorage.userid,
            method: 'get',
            headers: {
                Authorization: 'Bearer ' + localStorage.token,
            }
        }).then((response) => {
            var totalContact=response.data.data
            var newContact=totalContact.map(each=>({
                ...each, link:<Link to={'/editcontact/'+each.contactId}>Edit</Link>
               
              
            }))
            setContact(newContact)
            setLoading(false)

        })
    }, [localStorage.token, localStorage.userid])
    
     console.log(contact)

    useEffect(() => {
        const tabledata = {
        columns: [
          {
            label: 'First Name',
            field: 'first_name',
            width: 150,
            attributes: {
              'aria-controls': 'DataTable',
              'aria-label': 'Name',
            },
          },
          {
            label: 'Last Name',
            field: 'last_name',
            width: 270,
          },
          {
            label: 'Phone No',
            field: 'contact_phone_number',
            width: 200,
          },
          {
            label: 'Email',
            field: 'contact_email',
            sort: 'asc',
            width: 100,
          },
          {
            label: 'Nick Name',
            field: 'contact_nickname',
            sort: 'disabled',
            width: 150,
          },
          {
            label: 'Company',
            field: 'contact_company',
            sort: 'disabled',
            width: 100,
          },
          {
            label: 'Action',
            field: 'link',
            sort: 'disabled',
            width: 100,
          },
        ],
        rows:contact,
    };
        setData(tabledata);
    }, [contact]);
   
    
    

    return (
      <div>
        {loading?(<Spinner/>):
        (

    
        <main className="py-4">
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-8">
                        <div className="card">
                            <div className="card-header">Contact List |
                                <Link to="addcontact">Add Contact</Link>
                            </div>
                         {/* <MDBDataTableV5 hover entriesOptions={[5, 20, 25]} entries={5} pagesAmount={4} data={datatable} /> */}
                         <MDBDataTable striped bordered data={data} />
                         
                            <div className="card-body">
                             
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
            )}
        </div>
    );
}

export default Contact
