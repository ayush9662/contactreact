import { useState,useEffect } from 'react';
import axios from 'axios'
import {toast} from 'react-toastify';
import Spinner from "./UI/Spinner";
import 'react-toastify/dist/ReactToastify.css';
toast.configure()

function Register(props) {
    useEffect(() => {
        if (localStorage.token && localStorage.userid) {
            props.history.push('/dashboard');
        }
    }, [])
    const [input, setInput] = useState({ "first_name": '', "last_name": "", "phone": "", "password": "", "email": "" })
    const [error, setError] = useState({})
    const [loading,setLoading]=useState(false)

    const validation = (input) => {
        var errors = {}
        if (input.first_name === '') {
            errors.first = 'First name is required'
        }
        if (input.last_name === '') {
            errors.last = 'Last name is required'
        }
        if (input.phone === '') {
            errors.phone = 'Phone field is required'
        }
        else if (input.phone) {
            if (isNaN(input.phone))
                errors.phone = 'Please enter phone number in digit'
        }
        if (input.password === '') {
            errors.password = 'Password field is required'
        } else if (input.password) {
            if (input.password.length < 6) {
                errors.password = 'Please enter a six digit password'
            }
        }
        if (input.email === '') {
            errors.email = "Plaese enter email";
        } else if (input.email) {
            const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
            var isValid = pattern.test(input.email);
            if (!isValid) {
                errors.email = "Plaese enter valid email";
            }
        }
        var errorKeys = Object.keys(errors);
        if (errorKeys.length > 0) {
            return errors;
        } else {
            return false;
        }
    }
    const inputHandler =(e) => {
        setInput({ ...input, [e.target.name]: e.target.value })
        var valerror = validation(input)
        setError(valerror)
    }

    const formHandler = async  (e) => {
        e.preventDefault();

        var valerror = validation(input)
        if (valerror) {
            setError(valerror)
        }
        else {
            //APi call
            setLoading((true))
           const response=await axios({
                url:process.env.REACT_APP_BASE_URL+'/signup',
                data:input,
                method:"POST"
            })

              if(response.data.error_type=='validation'){
                
                if(response.data.errors.email){
                    toast.warning(response.data.errors.email[0]) 
                    setLoading(false)
                }
                if(response.data.errors.phone){
                    toast.warning(response.data.errors.phone[0]) 
                    setLoading(false)
                }
              }
              else if(response.data.error_type=='none'){
                toast.success(response.data.message) 
                props.history.push('/login')
              }
            
        }
    }

    return (
        <div>
            {loading?(<Spinner/>):(

        
        <main className="py-4">
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-8">
                        <div className="card">
                            <div className="card-header">Register</div>

                            <div className="card-body">
                                <form onSubmit={formHandler} >

                                    <div className="form-group row">
                                        <label for="first" className="col-md-4 col-form-label text-md-right">First Name</label>
                                        <div className="col-md-6">
                                            <input type="text" className="form-control" name="first_name" autofocus onChange={inputHandler} />
                                            <span style={{ color: "red" }}>{error?.first}</span>
                                        </div>
                                    </div>

                                    <div className="form-group row">
                                        <label for="last" className="col-md-4 col-form-label text-md-right">Last Name</label>
                                        <div className="col-md-6">
                                            <input type="text" className="form-control" name="last_name" onChange={inputHandler} />
                                            <span style={{ color: "red" }}>{error?.last}</span>
                                        </div>
                                    </div>

                                    <div className="form-group row">
                                        <label for="email" className="col-md-4 col-form-label text-md-right">E-Mail Address</label>
                                        <div className="col-md-6">
                                            <input type="email" className="form-control " name="email" onChange={inputHandler} />
                                            <span style={{ color: "red" }}>{error?.email}</span>
                                        </div>
                                    </div>

                                    <div className="form-group row">
                                        <label for="phone" className="col-md-4 col-form-label text-md-right">Phone no.</label>
                                        <div className="col-md-6">
                                            <input type="text" className="form-control " name="phone" maxLength="10" onChange={inputHandler} />
                                            <span style={{ color: "red" }}>{error?.phone}</span>
                                        </div>
                                    </div>

                                    <div className="form-group row">
                                        <label for="password" className="col-md-4 col-form-label text-md-right">Password</label>
                                        <div className="col-md-6">
                                            <input type="password" className="form-control " name="password" onChange={inputHandler} />
                                            <span style={{ color: "red" }}>{error?.password}</span>
                                        </div>
                                    </div>


                                    <div className="form-group row mb-0">
                                        <div className="col-md-8 offset-md-4">
                                            <button type="submit" className="btn btn-primary">
                                                Register
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
            )}
        </div>
    )
}

export default Register;