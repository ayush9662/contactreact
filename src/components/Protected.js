import {useEffect} from 'react'
import {useHistory} from 'react-router-dom'

function Protected(props)
{
    const history=useHistory();
    let Component=props.component
    useEffect(()=>{
        if (!localStorage.token && !localStorage.userid) {
            history.push('/login');
        }
    },[])

    return (
        <Component/>
    )
}

export default Protected