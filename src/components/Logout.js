import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
toast.configure()

function Logout(props) {
    props.logoutData({ isLogin: false, userid: '' })
    props.history.push('/login')
    // window.location.reload();
    return (
        <h1></h1>
    )
}

export default Logout