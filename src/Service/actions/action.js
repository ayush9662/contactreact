import {LOGIN} from '../constants'
import {LOGOUT} from '../constants'

export const login=(data)=>{
    return {
        type:LOGIN,
        data:data
    }

}

export const logout=(data)=>{
    return {
        type:LOGOUT,
        data:data
    }

}