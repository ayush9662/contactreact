//define initial state


import {LOGIN} from '../constants'
import {LOGOUT} from '../constants'

const initialState ={
    isLoggedIn: !!localStorage.token,
    userid: localStorage.userid ? localStorage.userid  : undefined,
    token: localStorage.token,
    isLoading: false,
    loginTime:'',
    expireTime:''
}
const d=new Date()

export default function authReducer(state=initialState, action) {
    switch (action.type) {
        case LOGIN:
            
            {
                state = {...state}
                state['token'] = action.data?.token
                state['isLoggedIn'] = true
                state['userid'] = action.data?.userid
                state['isLoading'] = true
                state['loginTime'] = d.getTime()
                state['expireTime']=state['loginTime']+120000
                return state
            }
        break;
        case LOGOUT:
            {
            state = {...state}
            state['token'] = localStorage.clear()
            state['isLoggedIn'] = false
            state['userid'] = undefined
            state['isLoading'] = false
            state['loginTime'] = ''
            state['expireTime']=''
            return state

            }
            
        break;
        default:
            return state

    }

}